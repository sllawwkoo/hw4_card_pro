import { Card, CurrencyEnum, Transaction, BonusCard, Pocket } from "./index";

describe("Card", () => {
	let card: Card;

	beforeEach(() => {
		card = new Card();
	});

	it("should add transactions and get correct balance", () => {
		// Додавання транзакцій
		const transactionId1 = card.addTransaction(
			new Transaction(100, CurrencyEnum.USD)
		);
		const transactionId2 = card.addTransaction(CurrencyEnum.UAH, 200);

		// Отримання транзакції за Id
		const transaction1 = card.getTransaction(transactionId1);
		expect(transaction1).toEqual(
			expect.objectContaining({ amount: 100, currency: CurrencyEnum.USD })
		);

		const transaction2 = card.getTransaction(transactionId2);
		expect(transaction2).toEqual(
			expect.objectContaining({ amount: 200, currency: CurrencyEnum.UAH })
		);

		// Отримання балансу за валютою
		const usdBalance = card.getBalance(CurrencyEnum.USD);
		expect(usdBalance).toBe(100);

		const uahBalance = card.getBalance(CurrencyEnum.UAH);
		expect(uahBalance).toBe(200);
	});
});

describe("BonusCard", () => {
	let card: BonusCard;

	beforeEach(() => {
		card = new BonusCard();
	});

	it("should add transactions with bonus and get correct balance", () => {
		// Додавання транзакцій
		const transactionId1 = card.addTransaction(
			new Transaction(100, CurrencyEnum.USD)
		);
		const transactionId2 = card.addTransaction(CurrencyEnum.UAH, 200);

		// Отримання транзакції за Id
		const transaction1 = card.getTransaction(transactionId1);
		expect(transaction1).toEqual(
			expect.objectContaining({ amount: 100, currency: CurrencyEnum.USD })
		);

		const transaction2 = card.getTransaction(transactionId2);
		expect(transaction2).toEqual(
			expect.objectContaining({ amount: 200, currency: CurrencyEnum.UAH })
		);

		// Отримання балансу за валютою
		const usdBalance = card.getBalance(CurrencyEnum.USD);
		expect(usdBalance).toBe(110); // 10% бонус до суми 100 USD

		const uahBalance = card.getBalance(CurrencyEnum.UAH);
		expect(uahBalance).toBe(220); // 10% бонус до суми 200 UAH
	});
});

describe("Pocket", () => {
	let pocket: Pocket;
	let cardStandart: Card;
	let cardBonus: BonusCard;

	beforeEach(() => {
		pocket = new Pocket();
		cardStandart = new Card();
		cardBonus = new BonusCard();

		// Додавання транзакцій до кожної картки
		cardStandart.addTransaction(CurrencyEnum.USD, 50);
		cardStandart.addTransaction(CurrencyEnum.UAH, 100);

		cardBonus.addTransaction(CurrencyEnum.USD, 50);
		cardBonus.addTransaction(CurrencyEnum.UAH, 100);

		// Додавання карток до кишені
		pocket.addCard("Standart", cardStandart);
		pocket.addCard("Bonus", cardBonus);
	});

	it("should remove card by name", () => {
		const removedCard = pocket.removeCard("Bonus");
		expect(removedCard).toBeUndefined();

		const cardByName = pocket.getCard("Bonus");
		expect(cardByName).toBeUndefined();
	});

	it("should get total amount in pocket", () => {
		const totalUsdAmount = pocket.getTotalAmount(CurrencyEnum.USD);
		expect(totalUsdAmount).toBe(105); // 50 USD від Standart і 50 USD від Bonus з бонусом

		const totalUahAmount = pocket.getTotalAmount(CurrencyEnum.UAH);
		expect(totalUahAmount).toBe(210); // 100 UAH від Standart і 100 UAH від Bonus
	});
});

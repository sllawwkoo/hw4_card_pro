import { v4 as uuidv4 } from "uuid";

// Enum для валют
export enum CurrencyEnum {
	USD = "USD",
	UAH = "UAH",
}

// Клас транзакції
export class Transaction {
	id: string;

	constructor(public amount: number, public currency: CurrencyEnum) {
		this.id = uuidv4();
	}
}

// Інтерфейс для класу картки
interface ICard {
	addTransaction(transaction: Transaction): string;
	getTransaction(id: string): Transaction | undefined;
	getBalance(currency: CurrencyEnum): number;
}

// Клас картки
export class Card implements ICard {
	public transactions: Transaction[] = [];

	// Метод додавання транзакції + перевантаження
	public addTransaction(transaction: Transaction): string;
	public addTransaction(currency: CurrencyEnum, amount: number): string;
	public addTransaction(
		arg1: Transaction | CurrencyEnum,
		arg2?: number
	): string {
		let transaction: Transaction;
		if (arg1 instanceof Transaction) {
			transaction = arg1;
		} else {
			transaction = new Transaction(arg2!, arg1);
		}
		this.transactions.push(transaction);
		return transaction.id;
	}

	// Метод отримання транзакції за Id
	public getTransaction(id: string): Transaction | undefined {
		return this.transactions.find((transaction) => transaction.id === id);
	}

	// Метод отримання балансу за вказаною валютою
	public getBalance(currency: CurrencyEnum): number {
		return this.transactions
			.filter((transaction) => transaction.currency === currency)
			.reduce((total, transaction) => total + transaction.amount, 0);
	}
}

export class BonusCard extends Card {
	
	public addTransaction(
		arg1: Transaction | CurrencyEnum,
		arg2?: number
	): string {
		let transaction: Transaction;
		if (arg1 instanceof Transaction) {
			transaction = arg1;
		} else {
			transaction = new Transaction(arg2!, arg1);
		}

		const bonusAmount = transaction.amount * 0.1; // 10% бонус
		const bonusTransaction = new Transaction(bonusAmount, transaction.currency);
		this.transactions.push(bonusTransaction); // Додаємо бонусну транзакцію
		this.transactions.push(transaction); // Додаємо звичайну транзакцію
		return transaction.id; // Повертаємо id звичайної транзакції
	}
}

export class Pocket {
	private cards: { name: string; card: ICard }[] = [];

	public addCard(name: string, card: ICard): void {
		this.cards.push({ name, card });
	}

	public removeCard(name: string): void {
		const index = this.cards.findIndex((card) => card.name === name);
		if (index !== -1) {
			this.cards.splice(index, 1);
		}
	}

	public getCard(name: string): ICard | undefined {
		const card = this.cards.find((card) => card.name === name);
		return card ? card.card : undefined;
	}

	public getAllCards(): { name: string; card: ICard }[] {
		return this.cards;
	}

	public getTotalAmount(currency: CurrencyEnum): number {
		let totalAmount = 0;
		for (const { card } of this.cards) {
			totalAmount += card.getBalance(currency);
		}
		return totalAmount;
	}
}

// Створення нової картки та додавання транзакцій
const cardStandart = new Card();
const cardBonus = new BonusCard();

const transactionId1 = cardStandart.addTransaction(CurrencyEnum.USD, 50);
const transactionId2 = cardStandart.addTransaction(CurrencyEnum.UAH, 100);

const transactionId3 = cardBonus.addTransaction(CurrencyEnum.USD, 50);
const transactionId4 = cardBonus.addTransaction(CurrencyEnum.UAH, 100);

// Отримання транзакцій за ID
const transaction1 = cardStandart.getTransaction(transactionId1);
const transaction2 = cardStandart.getTransaction(transactionId2);

const transaction3 = cardBonus.getTransaction(transactionId3);
const transaction4 = cardBonus.getTransaction(transactionId4);

console.log("Transaction 1:", transaction1);
console.log("Transaction 2:", transaction2);
console.log("Transaction 3:", transaction3);
console.log("Transaction 4:", transaction4);

// Отримання балансу за валютою
const usdBalance1 = cardStandart.getBalance(CurrencyEnum.USD);
const uahBalance1 = cardStandart.getBalance(CurrencyEnum.UAH);

const usdBalance2 = cardBonus.getBalance(CurrencyEnum.USD);
const uahBalance2 = cardBonus.getBalance(CurrencyEnum.UAH);

console.log("Card_Snandart Balance in USD:", usdBalance1);
console.log("Card_Snandart Balance in UAH:", uahBalance1);
console.log("Card_Bonus Balance in USD:", usdBalance2);
console.log("Card_Bonus Balance in UAH:", uahBalance2);

// Створення гаманець та додавання карток
const pocket = new Pocket();
pocket.addCard("Standart", cardStandart);
console.log("All cards after adding a card:", pocket.getAllCards());
pocket.addCard("Bonus", cardBonus);
console.log("All cards after adding a card:", pocket.getAllCards());

// Отримання загальної суми транзакцій усіх карток
const totalUsdAmount = pocket.getTotalAmount(CurrencyEnum.USD);
const totalUahAmount = pocket.getTotalAmount(CurrencyEnum.UAH);

console.log("Total amount in USD in pocket:", totalUsdAmount);
console.log("Total amount in UAH in pocket:", totalUahAmount);

// Отримання картки з гаманця за ім'ям
const cardByName = pocket.getCard("Standart");
console.log("Card by name 'Standart':", cardByName);

// Отримання картки з гаманця за ім'ям
const cardByName2 = pocket.getCard("Bonus");
console.log("Card by name 'Bonus':", cardByName2);

// Видалення картки з гаманця за ім'ям
const removedCard = pocket.removeCard("Bonus");
console.log("Removed card:", removedCard);

// Отримання загальної суми транзакцій усіх карток після видалення
const totalUsdAmountAfterRemove = pocket.getTotalAmount(CurrencyEnum.USD);
const totalUahAmountAfterRemove = pocket.getTotalAmount(CurrencyEnum.UAH);

console.log(
	"Total amount in USD in pocket after remove:",
	totalUsdAmountAfterRemove
);
console.log(
	"Total amount in UAH in pocket after remove:",
	totalUahAmountAfterRemove
);

// Результат роботи отримати в консолі = npm start
// Перевірити за допомогою тестування = npm run test
